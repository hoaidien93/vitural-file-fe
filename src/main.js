import Vue from "vue";
import App from "./App.vue";
import Vuesax from "vuesax";

import "vuesax/dist/vuesax.css";
// Vue Router
import router from "./router";

Vue.use(Vuesax);
import "./assets/styles/tailwind.css";

Vue.use(Vuesax, {
})
Vue.config.productionTip = false;

new Vue({
  render: (h) => h(App),
  router: router
}).$mount("#app");
