import Vue from "vue";
import Router from "vue-router";

Vue.use(Router);

const router = new Router({
  mode: "history",
  scrollBehavior() {
    return { x: 0, y: 0 };
  },
  routes: [
    {
      path: "/login",
      name: "page-login",
      component: () => import("./pages/Login"),
      meta: {
        requireLogin: false,
      },
    },
    {
      path: "/",
      name: "page-command-line",
      component: () => import("./pages/CommandLine"),
      meta: {
        requireLogin: true,
      },
    },
    {
      path: "/register",
      name: "page-login",
      component: () => import("./pages/Register"),
      meta: {
        requireLogin: false,
      },
    },
  ],
});

router.beforeEach((to, from, next) => {
  if (to.meta.requireLogin) {
    const token = localStorage.getItem("token");
    if (!token) {
      next({
        path: "/login",
      });
    }
    next();
  } else {
    next();
  }
});

export default router;
