import axios from "axios";
const BASE_URL = "http://54.179.70.143:3000";
const ACCESS_TOKEN = window.localStorage.getItem('token');
axios.defaults.headers.common["Authorization"] = `Bearer ${ACCESS_TOKEN}`;

export const loginAPI = (data) => {
  return axios.post(`${BASE_URL}/user/login`, data);
};

export const registerAPI = (data) => {
  return axios.post(`${BASE_URL}/user/register`, data);
};

export const getListFile = obj => {
  return axios.post(`${BASE_URL}/command/get-list-file`, obj);
}

export const setToken = (token) => {
  axios.defaults.headers.common["Authorization"] = `Bearer ${token}`;
}

export const changeDirectory = obj => {
  return axios.post(`${BASE_URL}/command/change-directory`, obj);
}

export const moveDirectory = obj => {
  return axios.post(`${BASE_URL}/command/move-storage`, obj);
}

export const removeStorage = obj => {
  return axios.post(`${BASE_URL}/command/remove-storage`, obj);
}


export const createStorageStorage = obj => {
  return axios.post(`${BASE_URL}/command/create-new-file`, obj);
}