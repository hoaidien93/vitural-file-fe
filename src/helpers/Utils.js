import {
    changeDirectory,
    moveDirectory,
    removeStorage,
    createStorageStorage,
    getListFile,
} from "../axios";
import { format } from "date-fns"
import Constants from "./Constants";
const getParameters = function (list) {
    const type = list[0];
    const options = list.reduce((a, b) => {
        if (b[0] === "-") {
            a[b.slice(1)] = true;
        }
        return a;
    }, {});
    const values = list.filter((e, i) => i !== 0 && e[0] !== "-");
    return {
        type,
        options,
        values,
    };
};

export const CommandFactory = (function () {
    const getInstance = function (command) {
        let instance;
        switch (checkType(command)) {
            case "cr":
                instance = new CreateCommand(command);
                break;
            case "cd":
                instance = new ChangeDirectoryCommand(command);
                break;
            case "mv":
                instance = new MoveStorageCommand(command);
                break;
            case "rm":
                instance = new RemoveStorageCommand(command);
                break;
            case "ls":
                instance = new ListFileCommand(command);
                break;
            default:
                instance = new ExceptionCommand(command); 
                break;
        }
        return instance;
    };

    const checkType = (command = "") => {
        return command.split` `.shift();
    };

    return {
        getInstance,
    };
})();

const isValidPath = (path, isFile) => {
    path = path.split("/");
    if (isFile) {
        const fileName = path.slice(-1);
        return (
            /^[\.a-zA-Z0-9 _-]+$/.test(fileName) &&
            path
                .slice(0, path.length - 1)
                .every((e) => !e || /^[a-zA-Z0-9 _-]+$/.test(e))
        );
    }
    return path.every((e) => !e || /^[a-zA-Z0-9 _-]+$/.test(e));
};

const CreateCommand = function (cmd) {
    const listParams = getParameters(cmd.split` `);
    this.send = async () => {
        const [path, ...data] = listParams.values;
        const isCreate = listParams.options.p || false;
        if (!path) throw "Error command";
        const absolutePath = relativeToAbsolute(path);
        if (isValidPath(absolutePath, !!data.length)) {
            const result = await createStorageStorage({
                path: absolutePath,
                data: data.join(" ") || null,
                isCreate,
            });
            if (result.data.code !== Constants.SUCCESS_CODE) {
                throw "Failed to create";
            }
        } else throw "Invalid folder/file name";

        return "Created";
    };
};

const ChangeDirectoryCommand = function (cmd) {
    const listParams = getParameters(cmd.split` `);
    this.send = async () => {
        const path = listParams.values?.[0];
        if (!path) throw "Error command";
        const absolutePath = relativeToAbsolute(path);
        const result = await changeDirectory({
            path: absolutePath,
        });
        if (result.data.code !== Constants.SUCCESS_CODE) {
            throw "No such file or directory";
        }
        localStorage.setItem("currentFolder", absolutePath);
        return "";
    };
};

const MoveStorageCommand = function (cmd) {
    const listParams = getParameters(cmd.split` `);
    this.send = async () => {
        const [desPath, ...srcArr] = listParams?.values
            ?.map((e) => relativeToAbsolute(e))
            .reverse();
        if (!desPath || srcArr.length === 0) throw "Error command";
        if (srcArr.some((e) => desPath.includes(e)))
            throw "Cannot move a folder to become a subfolder of itself";
        const result = await moveDirectory({
            source: srcArr,
            des: desPath,
        });
        if (result.data.code !== Constants.SUCCESS_CODE) {
            throw "Something went wrong !!!";
        }
        return "Moved";
    };
};

const RemoveStorageCommand = function (cmd) {
    const listParams = getParameters(cmd.split` `);
    this.send = async () => {
        const path = listParams.values?.[0];
        if (!path) throw "Error command";
        const absolutePath = relativeToAbsolute(path);
        const isRecursive = listParams.options.r || false;
        const result = await removeStorage({
            path: absolutePath,
            isRecursive,
        });
        if (result.data.code !== Constants.SUCCESS_CODE) {
            throw "No such file or directory";
        }
        return "Removed";
    };
};

const ListFileCommand = function (cmd) {
    const command = cmd;
    const listParams = getParameters(command.split` `);

    const listRender = (directory, isShowFullProps) => {
        const result = [];
        const formatData = (data, nested) => {
            let result = [nested ? " ".repeat(nested * 8) + (data.type === 'file'? "📃":"📁") : ""]
            if (!isShowFullProps) {
                result.push(data.name)
            } else {
                result.push(
                    data.name,
                    formatConsoleDateTime(data.created_at),
                    formatConsoleDateTime(data.updated_at),
                    data.size
                )
            }
            return result.join(" ".repeat(4))
        }
        const func = (directory, nested = 0) => {
            if (directory.type === "file") {
                result.push(formatData(directory, nested))
                return;
            }
            if (directory.type === "folder") {
                result.push(formatData(directory, nested))
                directory.listChildrens.forEach(e => func(e, nested + 1))
            }
        }

        func(directory, 0);

        return result;
    }
    this.send = async () => {
        const path = localStorage.getItem("currentFolder");
        const isLongList = listParams.options.l || false;
        const level = +listParams.values?.[0] || 1;
        if (level !== false && level < 1) throw "Level invalid";
        const result = await getListFile({ path, level, isLongList })
        if (result.data.code !== Constants.SUCCESS_CODE) {
            throw "Err"
        }
        const list = listRender(result.data.data, isLongList);
        return list;
    }
}

const ExceptionCommand = function (cmd){
    this.send = () => {
        throw("Command not found")
    }
}

export const relativeToAbsolute = function (relativePath) {
    let currentFolder = localStorage.getItem("currentFolder") || "/";
    if (relativePath[0] === "/") currentFolder = "/";
    let array = currentFolder.split("/");
    if (array.every((e) => !e)) array.length = 1;
    const relativePathArr = relativePath.split("/");
    for (let i = 0; i < relativePathArr.length; i++) {
        if (relativePathArr[i] === "..") {
            array.pop();
        } else {
            if (relativePathArr[i] != "" && relativePathArr[i] != ".") {
                array.push(relativePathArr[i]);
            }
        }
    }
    return array.join("/") || "/";
};

const formatConsoleDateTime = (datetime) => format(new Date(datetime), "hh:mm:ss dd/MM/yyyy")
